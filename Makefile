PROJECT_NAME=dad
PACKAGE=com.damienruscoe.android.dad
MAIN_ACTIVITY=com.damienruscoe.android.dad.MainActivity
DEBUG_APK=$(PROJECT_NAME)/build/outputs/apk/debug/$(PROJECT_NAME)-debug.apk
SOURCE_DIR=$(PROJECT_NAME)/src/main/java/com/damienruscoe/android/dad/
SOURCE_FILES := $(shell find $(PROJECT_NAME)/src -type f)

.PHONY : all
all : build deploy launch

.PHONY: build
build: $(DEBUG_APK)

.PHONY: deploy
deploy: $(DEBUG_APK).deployed

.PHONY: publish
publish: $(DEBUG_APK)
	scp $(PROJECT_NAME)/build/outputs/apk/debug/$(PROJECT_NAME)-debug.apk home:/mnt/spare/apks/dad.debug.apk

.PHONY: uninstall
uninstall: 
	adb uninstall $(PACKAGE)
	rm -f $(DEBUG_APK).deployed

.PHONY: launch

.PHONY: llog
llog: launch log

.PHONY: log
log:
	#adb -d logcat $(PACKAGE):I *:S
	adb -d logcat | less +F



$(DEBUG_APK): $(SOURCE_FILES)
	./gradlew build
	rm -f $(DEBUG_APK).deployed

$(DEBUG_APK).deployed:
	adb install -r $(DEBUG_APK)
	touch $(DEBUG_APK).deployed

launch: $(DEBUG_APK).deployed
	#adb shell am start -n $(PACKAGE)/$(MAIN_ACTIVITY)
	adb shell monkey -p $(PACKAGE) -c android.intent.category.LAUNCHER 1

clean:
	./gradlew clean
	rm -f $(DEBUG_APK).deployed
