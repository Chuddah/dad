package com.damienruscoe.android.dad.activities;

import android.content.Context;
import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;

import java.lang.Runtime;
import java.io.IOException;
import android.util.Log;
import android.os.PowerManager;
import android.support.v4.content.ContextCompat;

import com.damienruscoe.android.dad.R;
import com.damienruscoe.android.dad.Webserver;
import com.damienruscoe.android.dad.CurrentActivity;
import com.damienruscoe.android.dad.lib.MqttUtils;
import com.damienruscoe.android.dad.lib.AndroidUtils;

import android.content.Intent;
import android.net.Uri;
import android.content.pm.PackageInstaller;
import android.app.PendingIntent;

/*
import android.app.ActivityTaskManager;
import android.app.IActivityManager;
import android.app.TaskStackListener;
*/

public class MainActivity extends Activity {

    public static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1 = (Button) findViewById(R.id.button1);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AndroidUtils.start_kodi(MainActivity.this);
            }
        });

        Button b2 = (Button) findViewById(R.id.button2);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AndroidUtils.start_termux(MainActivity.this);
            }
        });

        Button b3 = (Button) findViewById(R.id.button3);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Runtime.getRuntime().exec(new String[] {"/system/xbin/su", "-c", "reboot now"});
                } catch (Exception e) {
                    //Log.e(TAG, e.message);
                    e.printStackTrace();
                }
                // The following needs "system" permissions
                // See: https://stackoverflow.com/questions/32984849/restarting-a-device-programmatically
                //PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                //pm.reboot(null);
            }
        });

        Button b4 = (Button) findViewById(R.id.button4);
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    new Webserver(MainActivity.this);
                } catch (IOException ioe) {
                    System.err.println("Couldn't start server:\n" + ioe);
                }
            }
        });

        Button b5 = (Button) findViewById(R.id.button5);
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MqttUtils.wibble(MainActivity.this);
            }
        });

        Button b6 = (Button) findViewById(R.id.button6);
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CurrentActivity.print_current(MainActivity.this);
            }
        });

        Button b7 = (Button) findViewById(R.id.button7);
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // android.permission.REQUEST_DELETE_PACKAGES
                // Ask the OS to unistall this app on our behalf
                /*
                Intent intent = new Intent(Intent.ACTION_DELETE);
                intent.setData(Uri.parse("package:com.damienruscoe.android.dad"));
                startActivity(intent);
                */

                // android.permission.DELETE_PACKAGES
                // Do this uninstall ourselves. This also requires Device Admin permissions.
                /*
                String package_name = "com.damienruscoe.android.dad";
                Activity activity = MainActivity.getActivity();

                Intent intent = new Intent(activity, activity.getClass());
                //intent.setData(Uri.parse("package:com.damienruscoe.android.dad"));
                PendingIntent sender = PendingIntent.getActivity(activity, 0, intent, 0);

                PackageInstaller mPackageInstaller = activity.getPackageManager().getPackageInstaller();
                mPackageInstaller.uninstall(package_name, sender.getIntentSender());
                */


                Context context = MainActivity.this;
                //private static 
                final String ACTION_UNINSTALL_COMPLETE = "com.afwsamples.testdpc.UNINSTALL_COMPLETE";

                //String package_name = "com.damienruscoe.android.dad";
                String package_name = "uk.co.aifactory.chessfree";

                final Intent intent = new Intent(ACTION_UNINSTALL_COMPLETE);
                intent.putExtra(Intent.EXTRA_PACKAGE_NAME, package_name);
                final PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);

                PackageInstaller mPackageInstaller = context.getPackageManager().getPackageInstaller();
                mPackageInstaller.uninstall(package_name, sender.getIntentSender());
            }
        });

        Button b8 = (Button) findViewById(R.id.button8);
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                ActivityTaskManager atm = (ActivityTaskManager) getSystemService(Context.ACTIVITY_TASK_SERVICE);
                atm.registerTaskStackListener(new TaskStackListener() {
                    @Override
                    public void onTaskStackChanged() throws RemoteException {
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                        Log.e(TAG, "TASK STACK CHANGED ===================================================================");
                    }

                });
                */
            }
        });
    }
}
