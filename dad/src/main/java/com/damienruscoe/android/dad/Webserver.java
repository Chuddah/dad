package com.damienruscoe.android.dad;

import java.io.IOException;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;
//import org.nanohttpd.NanoHTTPD; // NanoHTTPD >= 3.0.0

import java.util.List;
import java.util.Set;
import java.util.Arrays;
import android.app.ActivityManager;
import android.content.Context;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.os.Build;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.BatteryManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.Manifest;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;

import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.function.Function;
import android.app.usage.UsageStats;
import android.app.AppOpsManager;
import java.util.Formatter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteOrder;

import fi.iki.elonen.NanoHTTPD.Response.Status;

public class Webserver extends NanoHTTPD {

    private Context context;

	public Webserver(final Context context) throws IOException {
		super(7080);
        this.context = context;
		start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
		System.out.println("\nRunning! Point your browsers to http://localhost:7080/ \n");

        add_list_results_response("/api/navigation.json", this::get_registered_pages);
        add_map_results_response("/api/system_info.json", Webserver::get_system_info);
        add_list_results_response("/api/permissions.json", Webserver::get_permissions);
        add_map_results_response("/api/battery_info.json", Webserver::get_battery_info);
        add_map_results_response("/api/platform_info.json", Webserver::get_platform_info);
        add_map_results_response("/api/network_info.json", Webserver::get_network_info);
        add_map_results_response("/api/wifi_info.json", Webserver::get_wifi_info);
        add_map_results_response("/api/docking_info.json", Webserver::get_docking_info);
        add_list_results_response("/api/bluetooth_devices.json", Webserver::get_bluetooth_devices);
        add_list_results_response("/api/installed_apps.json", Webserver::get_installed_apps);
        add_list_results_response("/api/launcher_apps.json", Webserver::get_launcher_apps);
        add_list_results_response("/api/running_activities.json", Webserver::get_running_activities);

        site.put("/", new ResponseCallback() {
            @Override
            public Response get() throws Exception {
                String msg = "";
                msg += "<html><body>\n";
                msg += "<h1>Damiens Android Device</h1>\n";
                msg += "<h2>Whos the daddy</h1>\n";
                msg += "<a href='/api/navigation.json'>Navigation</a><br />\n";
                msg += "<br />\n";
                msg += "<a href='/api/system_info.json'>System Info</a><br />\n";
                msg += "<a href='/api/permissions.json'>Permissions</a><br />\n";
                msg += "<a href='/api/platform_info.json'>Platform Info</a><br />\n";
                msg += "<a href='/api/battery_info.json'>Battery Info</a><br />\n";
                msg += "<a href='/api/network_info.json'>Network Info</a><br />\n";
                msg += "<a href='/api/wifi_info.json'>WiFi Info</a><br />\n";
                msg += "<a href='/api/docking_info.json'>Docking Info</a><br />\n";
                msg += "<a href='/api/bluetooth_devices.json'>Bluetooth Devices</a><br />\n";
                msg += "<br />\n";
                msg += "<a href='/api/installed_apps.json'>Installed Apps</a><br />\n";
                msg += "<a href='/api/launcher_apps.json'>Launcher Apps</a><br />\n";
                msg += "<a href='/api/running_activities.json'>Running Activities</a><br />\n";
                msg += "</body></html>\n";
                return newFixedLengthResponse(Status.OK, "text/html", msg);
            }
        });

	}


    public JSONArray get_registered_pages(Context context) {
        JSONArray links = new JSONArray();
        try {
            for (Map.Entry<String,ResponseCallback> entry : site.entrySet()) {
                JSONObject item = new JSONObject();
                item.put("uri", entry.getKey());
                links.put(item);
            }
        } catch (Exception e) {
        }
        return links;
    }

    public static JSONArray get_running_activities(Context context) {
        List<UsageStats> apps = CurrentActivity.print_current(context);

        JSONArray info = new JSONArray();
        for (UsageStats app: apps) {
            try {
                JSONObject app_info = new JSONObject();
                Date last_used = new Date(app.getLastTimeUsed());
                app_info.put("package", app.getPackageName());
                app_info.put("last_used", last_used);
                info.put(app_info);
            } catch (JSONException e) {
            }
        }
        return info;
    }

    public static boolean check_self_permission(Context context, String permission) {
        //int result = ContextCompat.checkSelfPermission(context, permission);
        int result = context.checkSelfPermission(permission);
        boolean granted = (result != PackageManager.PERMISSION_DENIED);
        return granted;
    }
    public static boolean check_appops_permission(Context context, String permission) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(permission, android.os.Process.myUid(), context.getPackageName());
        boolean granted = (mode == AppOpsManager.MODE_ALLOWED);
        return granted;
    }


    public static JSONArray get_permissions(Context context) {
        List<String> app_op_permissions = Arrays.asList(
            AppOpsManager.OPSTR_GET_USAGE_STATS
        );
        List<String> permissions = Arrays.asList(
            //Manifest.permission.ACCEPT_HANDOVER,
            //Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            Manifest.permission.ACCESS_CHECKIN_PROPERTIES,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS,
            //Manifest.permission.ACCESS_MEDIA_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_NOTIFICATION_POLICY,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCOUNT_MANAGER,
            //Manifest.permission.ACTIVITY_RECOGNITION,
            Manifest.permission.ADD_VOICEMAIL,
            //Manifest.permission.ANSWER_PHONE_CALLS,
            Manifest.permission.BATTERY_STATS,
            Manifest.permission.BIND_ACCESSIBILITY_SERVICE,
            Manifest.permission.BIND_APPWIDGET,
            //Manifest.permission.BIND_AUTOFILL_SERVICE,
            //Manifest.permission.BIND_CALL_REDIRECTION_SERVICE,
            //Manifest.permission.BIND_CARRIER_MESSAGING_CLIENT_SERVICE,
            Manifest.permission.BIND_CARRIER_MESSAGING_SERVICE,
            Manifest.permission.BIND_CARRIER_SERVICES,
            Manifest.permission.BIND_CHOOSER_TARGET_SERVICE,
            Manifest.permission.BIND_CONDITION_PROVIDER_SERVICE,
            //Manifest.permission.BIND_CONTROLS,
            Manifest.permission.BIND_DEVICE_ADMIN,
            Manifest.permission.BIND_DREAM_SERVICE,
            Manifest.permission.BIND_INCALL_SERVICE,
            Manifest.permission.BIND_INPUT_METHOD,
            Manifest.permission.BIND_MIDI_DEVICE_SERVICE,
            Manifest.permission.BIND_NFC_SERVICE,
            Manifest.permission.BIND_NOTIFICATION_LISTENER_SERVICE,
            Manifest.permission.BIND_PRINT_SERVICE,
            //Manifest.permission.BIND_QUICK_ACCESS_WALLET_SERVICE,
            Manifest.permission.BIND_QUICK_SETTINGS_TILE,
            Manifest.permission.BIND_REMOTEVIEWS,
            Manifest.permission.BIND_SCREENING_SERVICE,
            Manifest.permission.BIND_TELECOM_CONNECTION_SERVICE,
            Manifest.permission.BIND_TEXT_SERVICE,
            Manifest.permission.BIND_TV_INPUT,
            //Manifest.permission.BIND_VISUAL_VOICEMAIL_SERVICE,
            Manifest.permission.BIND_VOICE_INTERACTION,
            Manifest.permission.BIND_VPN_SERVICE,
            Manifest.permission.BIND_VR_LISTENER_SERVICE,
            Manifest.permission.BIND_WALLPAPER,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.BLUETOOTH_PRIVILEGED,
            Manifest.permission.BODY_SENSORS,
            Manifest.permission.BROADCAST_PACKAGE_REMOVED,
            Manifest.permission.BROADCAST_SMS,
            Manifest.permission.BROADCAST_STICKY,
            Manifest.permission.BROADCAST_WAP_PUSH,
            //Manifest.permission.CALL_COMPANION_APP,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.CALL_PRIVILEGED,
            Manifest.permission.CAMERA,
            Manifest.permission.CAPTURE_AUDIO_OUTPUT,
            Manifest.permission.CHANGE_COMPONENT_ENABLED_STATE,
            Manifest.permission.CHANGE_CONFIGURATION,
            Manifest.permission.CHANGE_NETWORK_STATE,
            Manifest.permission.CHANGE_WIFI_MULTICAST_STATE,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.CLEAR_APP_CACHE,
            Manifest.permission.CONTROL_LOCATION_UPDATES,
            Manifest.permission.DELETE_CACHE_FILES,
            Manifest.permission.DELETE_PACKAGES,
            Manifest.permission.DIAGNOSTIC,
            Manifest.permission.DISABLE_KEYGUARD,
            Manifest.permission.DUMP,
            Manifest.permission.EXPAND_STATUS_BAR,
            Manifest.permission.FACTORY_TEST,
            //Manifest.permission.FOREGROUND_SERVICE,
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.GET_ACCOUNTS_PRIVILEGED,
            Manifest.permission.GET_PACKAGE_SIZE,
            Manifest.permission.GET_TASKS,
            Manifest.permission.GLOBAL_SEARCH,
            Manifest.permission.INSTALL_LOCATION_PROVIDER,
            Manifest.permission.INSTALL_PACKAGES,
            Manifest.permission.INSTALL_SHORTCUT,
            //Manifest.permission.INSTANT_APP_FOREGROUND_SERVICE,
            //Manifest.permission.INTERACT_ACROSS_PROFILES,
            Manifest.permission.INTERNET,
            Manifest.permission.KILL_BACKGROUND_PROCESSES,
            Manifest.permission.LOCATION_HARDWARE,
            Manifest.permission.MANAGE_DOCUMENTS,
            //Manifest.permission.MANAGE_EXTERNAL_STORAGE,
            //Manifest.permission.MANAGE_OWN_CALLS,
            Manifest.permission.MASTER_CLEAR,
            Manifest.permission.MEDIA_CONTENT_CONTROL,
            Manifest.permission.MODIFY_AUDIO_SETTINGS,
            Manifest.permission.MODIFY_PHONE_STATE,
            Manifest.permission.MOUNT_FORMAT_FILESYSTEMS,
            Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
            Manifest.permission.NFC,
            //Manifest.permission.NFC_PREFERRED_PAYMENT_INFO,
            //Manifest.permission.NFC_TRANSACTION_EVENT,
            Manifest.permission.PACKAGE_USAGE_STATS,
            Manifest.permission.PERSISTENT_ACTIVITY,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
            //Manifest.permission.QUERY_ALL_PACKAGES,
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_INPUT_STATE,
            Manifest.permission.READ_LOGS,
            //Manifest.permission.READ_PHONE_NUMBERS,
            Manifest.permission.READ_PHONE_STATE,
            //Manifest.permission.READ_PRECISE_PHONE_STATE,
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_SYNC_SETTINGS,
            Manifest.permission.READ_SYNC_STATS,
            Manifest.permission.READ_VOICEMAIL,
            Manifest.permission.REBOOT,
            Manifest.permission.RECEIVE_BOOT_COMPLETED,
            Manifest.permission.RECEIVE_MMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.RECEIVE_WAP_PUSH,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.REORDER_TASKS,
            //Manifest.permission.REQUEST_COMPANION_RUN_IN_BACKGROUND,
            //Manifest.permission.REQUEST_COMPANION_USE_DATA_IN_BACKGROUND,
            //Manifest.permission.REQUEST_DELETE_PACKAGES,
            Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
            Manifest.permission.REQUEST_INSTALL_PACKAGES,
            //Manifest.permission.REQUEST_PASSWORD_COMPLEXITY,
            Manifest.permission.RESTART_PACKAGES,
            Manifest.permission.SEND_RESPOND_VIA_MESSAGE,
            Manifest.permission.SEND_SMS,
            Manifest.permission.SET_ALARM,
            Manifest.permission.SET_ALWAYS_FINISH,
            Manifest.permission.SET_ANIMATION_SCALE,
            Manifest.permission.SET_DEBUG_APP,
            Manifest.permission.SET_PREFERRED_APPLICATIONS,
            Manifest.permission.SET_PROCESS_LIMIT,
            Manifest.permission.SET_TIME,
            Manifest.permission.SET_TIME_ZONE,
            Manifest.permission.SET_WALLPAPER,
            Manifest.permission.SET_WALLPAPER_HINTS,
            Manifest.permission.SIGNAL_PERSISTENT_PROCESSES,
            //Manifest.permission.SMS_FINANCIAL_TRANSACTIONS,
            //Manifest.permission.START_VIEW_PERMISSION_USAGE,
            Manifest.permission.STATUS_BAR,
            Manifest.permission.SYSTEM_ALERT_WINDOW,
            Manifest.permission.TRANSMIT_IR,
            Manifest.permission.UNINSTALL_SHORTCUT,
            Manifest.permission.UPDATE_DEVICE_STATS,
            //Manifest.permission.USE_BIOMETRIC,
            Manifest.permission.USE_FINGERPRINT,
            //Manifest.permission.USE_FULL_SCREEN_INTENT,
            Manifest.permission.USE_SIP,
            Manifest.permission.VIBRATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.WRITE_APN_SETTINGS,
            Manifest.permission.WRITE_CALENDAR,
            Manifest.permission.WRITE_CALL_LOG,
            Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_GSERVICES,
            Manifest.permission.WRITE_SECURE_SETTINGS,
            Manifest.permission.WRITE_SETTINGS,
            Manifest.permission.WRITE_SYNC_SETTINGS,
            Manifest.permission.WRITE_VOICEMAIL
        );

        JSONArray info = new JSONArray();
        try {
            for (String permission: app_op_permissions) {
                boolean success = true;
                boolean granted = false;
                try {
                    granted = check_appops_permission(context, permission);
                } catch (IllegalArgumentException e) {
                    success = false;
                }
                if (success) {
                    JSONObject restriction = new JSONObject();
                    restriction.put("id", permission);
                    restriction.put("protection", "AppOpsManager");
                    restriction.put("granted", granted);
                    info.put(restriction);
                }
            }

            for (String permission: permissions) {
                boolean success = true;
                boolean granted = false;
                try {
                    granted = check_self_permission(context, permission);
                } catch (IllegalArgumentException e) {
                    success = false;
                }
                if (success) {
                    JSONObject restriction = new JSONObject();
                    restriction.put("id", permission);
                    restriction.put("protection", "checkSelfPermission");
                    restriction.put("granted", granted);
                    info.put(restriction);
                }
            }
        } catch (JSONException e) {
            // Safely ignore.
            // Will only throw if JSONObject(key, ...) is null
        }
        return info;
    }

    public static JSONArray get_installed_apps(Context context) {
        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        List<ResolveInfo> apps = pm.queryIntentActivities(intent, 0);

        JSONArray applications = new JSONArray();
        for (ResolveInfo app : apps) {
            try {
                JSONObject app_info = new JSONObject();
                app_info.put("label", app.loadLabel(pm));
                app_info.put("package_name", app.activityInfo.applicationInfo.packageName);
                app_info.put("activity_name", app.activityInfo.name);
                applications.put(app_info);
            } catch (JSONException e) {
            }
        }

        return applications;
    }

    public static JSONArray get_launcher_apps(Context context) {
        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> apps = pm.queryIntentActivities(intent, 0);

        JSONArray applications = new JSONArray();
        for (ResolveInfo app : apps) {
            try {
                JSONObject app_info = new JSONObject();
                app_info.put("label", app.loadLabel(pm));
                app_info.put("package_name", app.activityInfo.applicationInfo.packageName);
                app_info.put("activity_name", app.activityInfo.name);
                applications.put(app_info);
            } catch (JSONException e) {
            }
        }

        return applications;
    }

    public static JSONObject get_platform_info(Context context) {
        JSONObject info = new JSONObject();
        try {
            info.put("Manufacturer", Build.MANUFACTURER);
            info.put("model", Build.MODEL);
            info.put("sdk_version", Build.VERSION.SDK_INT);
            info.put("sdk_release", Build.VERSION.RELEASE);
        } catch (JSONException e) {
        }
        return info;
    }
    
    public static JSONObject get_network_info(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();


        JSONObject info = new JSONObject();
        try {
            //info.put("address", InetAddress.getAddress());
            //info.put("hostname", InetAddress.getHostName());
            //info.put("canonical_hostname", InetAddress.getCanonicalHostName());
            info.put("metered", cm.isActiveNetworkMetered());
            info.put("available", activeNetwork != null && activeNetwork.isAvailable());
            info.put("connected", activeNetwork != null && activeNetwork.isConnected());
            info.put("connected_or_connecting", activeNetwork != null && activeNetwork.isConnectedOrConnecting());
            info.put("roaming", activeNetwork != null && activeNetwork.isRoaming());
        } catch (JSONException e) {
        }
        return info;
    }

    public static JSONObject get_wifi_info(Context context) {
        String ssid = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean connected = networkInfo.isConnected();

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo connection = wifiManager.getConnectionInfo();

        int ip = connection.getIpAddress();
        ip = (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) ?
                        Integer.reverseBytes(ip) : ip;

        byte[] ip_bytes = java.math.BigInteger.valueOf(ip).toByteArray();

        JSONObject info = new JSONObject();
        try {
            info.put("connected", connected);
            if (connection != null) {
                try {
                    info.put("ip_address", InetAddress.getByAddress(ip_bytes).getHostAddress());
                    info.put("hostname", InetAddress.getByAddress(ip_bytes).getHostName());
                } catch (UnknownHostException e) {
                }
                info.put("SSID", connection.getSSID());
                info.put("BSSID", connection.getBSSID());
                info.put("frequency", connection.getFrequency());
                info.put("hidden_ssid", connection.getHiddenSSID());
                info.put("ip_address_dec", ip);
                info.put("link_speed", connection.getLinkSpeed());
                info.put("mac_address", connection.getMacAddress());
                info.put("network_id", connection.getNetworkId());
                //info.put("passpoint_fqdn", connection.getPasspointFqdn());
                //info.put("passpoint_provider_friendly_name", connection.getPasspointProviderFriendlyName());
                info.put("rssi", connection.getRssi());
                //info.put("rx_link_speed_mbps", connection.getRxLinkSpeedMbps());
                //info.put("tx_link_speed_mbps", connection.getTxLinkSpeedMbps());
                //info.put("wifi_standard", connection.getWifiStandard());
            }
        } catch (JSONException e) {
        }

        return info;
    }

    public static JSONArray get_bluetooth_devices(Context context) {
        BluetoothManager manager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter adapter = manager.getAdapter();

        Set<BluetoothDevice> paired = adapter.getBondedDevices();
        //adapter.isEnabled()
        //adapter.enable()
        
        JSONArray info = new JSONArray();
        try {
            for(BluetoothDevice bt : paired) {
                JSONObject device = new JSONObject();
                BluetoothClass bt_class = bt.getBluetoothClass();
                //device.put("alias", bt.getAlias());
                device.put("name", bt.getName());
                device.put("type", bt.getType());
                device.put("address", bt.getAddress());
                device.put("class", bt_class);
                device.put("device_class", bt_class.getDeviceClass());
                device.put("major_device_class", bt_class.getMajorDeviceClass());
                device.put("has_audio", bt_class.hasService(BluetoothClass.Service.AUDIO));
                device.put("has_capture", bt_class.hasService(BluetoothClass.Service.CAPTURE));
                device.put("has_information", bt_class.hasService(BluetoothClass.Service.INFORMATION));
                device.put("has_limited_discoverability", bt_class.hasService(BluetoothClass.Service.LIMITED_DISCOVERABILITY));
                device.put("has_networking", bt_class.hasService(BluetoothClass.Service.NETWORKING));
                device.put("has_object_transfer", bt_class.hasService(BluetoothClass.Service.OBJECT_TRANSFER));
                device.put("has_render", bt_class.hasService(BluetoothClass.Service.RENDER));
                device.put("has_telephony", bt_class.hasService(BluetoothClass.Service.TELEPHONY));
                device.put("bond_state", bt.getBondState());
                info.put(device);
            }
        } catch (JSONException e) {
        }

        return info;
    }

    public static JSONObject get_battery_info(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        boolean isCharging = false;
        boolean usbCharge = false;
        boolean acCharge = false;
        float batteryPct = 0;

        if (batteryStatus != null) {
            // Are we charging / charged?
            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                         status == BatteryManager.BATTERY_STATUS_FULL;

            // How are we charging?
            int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

            // Determine current charge level
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            batteryPct = level * 100 / (float)scale;
        }

        JSONObject info = new JSONObject();
        try {
            info.put("has_battery", batteryStatus != null);
            info.put("charging", isCharging);
            info.put("usb_charging", usbCharge);
            info.put("ac_charging", acCharge);
            info.put("percentage", batteryPct);

        } catch (JSONException e) {
        }

        return info;
    }

    public static JSONObject get_docking_info(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_DOCK_EVENT);
        Intent dockStatus = context.registerReceiver(null, ifilter);

        boolean isDocked = false;
        boolean isCar = false;
        boolean isDesk = false;

        if (dockStatus != null) {
            int dockState = dockStatus.getIntExtra(Intent.EXTRA_DOCK_STATE, -1);
            isDocked = dockState != Intent.EXTRA_DOCK_STATE_UNDOCKED;

            isCar = dockState == Intent.EXTRA_DOCK_STATE_CAR;
            isDesk = dockState == Intent.EXTRA_DOCK_STATE_DESK ||
                             dockState == Intent.EXTRA_DOCK_STATE_DESK;
                            // API Level: 11+
                             //dockState == Intent.EXTRA_DOCK_STATE_LE_DESK ||
                             //dockState == Intent.EXTRA_DOCK_STATE_HE_DESK;
        }

        JSONObject info = new JSONObject();
        try {
            info.put("docked", isDocked);
            info.put("car", isCar);
            info.put("desk", isDesk);

        } catch (JSONException e) {
        }
        return info;
    }

    public static JSONObject get_system_info(Context context) {
        JSONObject info = new JSONObject();
        try {
            info.put("platform", get_platform_info(context));
            info.put("network", get_network_info(context));
            info.put("wifi", get_wifi_info(context));
            info.put("battery", get_battery_info(context));
            info.put("dock", get_docking_info(context));
            info.put("bluetooth_devices", get_bluetooth_devices(context));

        } catch (JSONException e) {
        }

        return info;
    }


    public void add_list_results_response(String uri, final Function<Context, JSONArray> callback) {
        site.put(uri, new ResponseCallback() {
            @Override
            public Response get() throws Exception {
                JSONObject result = new JSONObject();
                JSONArray details = callback.apply(context);
                result.put("status", "OK");
                result.put("count", details.length());
                result.put("results", details);
                return newFixedLengthResponse(Status.OK, "text/json", result.toString());
            }
        });
    }
    public void add_map_results_response(String uri, final Function<Context, JSONObject> callback) {
        site.put(uri, new ResponseCallback() {
            @Override
            public Response get() throws Exception {
                JSONObject result = callback.apply(context);
                return newFixedLengthResponse(Status.OK, "text/json", result.toString());
            }
        });
    }


    private Response createError(IHTTPSession session, Status status, String error) {
        Method method = session.getMethod();
        String uri = session.getUri();

        JSONObject result = new JSONObject();
        try {
            result.put("status", "ERROR");
            result.put("method", method);
            result.put("uri", uri);
            result.put("error", error);
        } catch (JSONException j) {
        }
        return newFixedLengthResponse(status, "text/json", result.toString());
    }

	@Override
	public Response serve(IHTTPSession session) {
        Method method = session.getMethod();
        String uri = session.getUri();

        try {
            return site.get(uri).get();
        } catch (NullPointerException e) {
            // 404 Error // NullPointerException from site.get(UNKNOWN)
            return createError(session, Status.NOT_FOUND, e.toString());
        } catch (Exception e) {
            // TODO: allow .get() callback to throw. We will catch here and report errors
            return createError(session, Status.INTERNAL_ERROR, e.toString());
        }
	}

    private abstract class ResponseCallback {
        public abstract Response get() throws Exception;
    }

    Map<String, ResponseCallback> site = new HashMap();
}
