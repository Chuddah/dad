package com.damienruscoe.android.dad.recievers;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.util.Log;

import com.damienruscoe.android.dad.lib.AndroidUtils;
import com.damienruscoe.android.dad.lib.MqttUtils;

public class BootReceiver extends BroadcastReceiver {

    static String Topic = "android/phone/power";

    @Override
    public void onReceive(Context context, Intent intent) {
        //if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
        //}
        MqttUtils.publish(context, Topic, "on");
        AndroidUtils.start_kodi(context);
    }

}

