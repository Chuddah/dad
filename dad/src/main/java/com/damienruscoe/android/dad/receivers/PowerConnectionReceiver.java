package com.damienruscoe.android.dad.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.os.BatteryManager;

import com.damienruscoe.android.dad.lib.MqttUtils;

public class PowerConnectionReceiver extends BroadcastReceiver {

    static String TAG = "PowerConnectionReceiver";
    static String Topic = "android/phone/test";

    @Override
    public void onReceive(Context context, Intent intent) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        if (batteryStatus != null) {
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            float battery_percentage = level * 100 / (float)scale;
            MqttUtils.publish(context, Topic, String.valueOf(battery_percentage));
        }
    }

}

