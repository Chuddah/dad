package com.damienruscoe.android.dad.lib;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

//import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
//import org.eclipse.paho.client.mqttv3.MqttCallback;
//import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.android.service.MqttAndroidClient;
//import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.BatteryManager;


public class MqttUtils {

    static String TAG = "MqttUtils";
	//static String Broker = "tcp://192.168.0.49:1883";
	static String Broker = "tcp://mqtt.damienruscoe.co.uk:1883";
	static String User = "druscoe";

	public static void publish(Context context, final String topic, final String message) {
		MemoryPersistence memPer = new MemoryPersistence();
		//final MqttAndroidClient client = new MqttAndroidClient(context, Broker, User, memPer);
		final MqttAndroidClient client = new MqttAndroidClient(context.getApplicationContext(), Broker, User, memPer);

        try {
			client.connect(null, new IMqttActionListener() {
				@Override
				public void onSuccess(IMqttToken mqttToken) {
					MqttMessage packet = new MqttMessage(message.getBytes());
					packet.setQos(2);
					packet.setRetained(false);

					try {
						client.publish(topic, packet);
						client.disconnect();

					} catch (MqttPersistenceException e) {
						e.printStackTrace();
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(IMqttToken arg0, Throwable arg1) {
					Log.i(TAG, "Client connection failed: "+arg1.getMessage());
				}
			});
        } catch (MqttException e) {
            e.printStackTrace();
        }
	}

	public static void wibble(Context context) {
        publish(context, "android/phone/test", "Wibble");
	}
}

