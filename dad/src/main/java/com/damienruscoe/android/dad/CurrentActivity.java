package com.damienruscoe.android.dad;

import java.util.List;
import java.util.ArrayList;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import com.jaredrummler.android.processes.AndroidProcesses;
import com.jaredrummler.android.processes.models.AndroidAppProcess;
import com.jaredrummler.android.processes.models.Stat;
import com.jaredrummler.android.processes.models.Statm;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.io.IOException;
import java.util.Date;
import java.util.Comparator;
import java.util.Collections;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.app.AppOpsManager;

import java.io.File;
import java.io.FileReader;

public class CurrentActivity {

    public static final String TAG = "CurrentActivity";

    public static List<UsageStats> print_current(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        //int mode = appOps.checkOpNoThrow("android:get_usage_stats", android.os.Process.myUid(), context.getPackageName());
        int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, android.os.Process.myUid(), context.getPackageName());

        List<UsageStats> result = new ArrayList();
        if (mode != AppOpsManager.MODE_ALLOWED) {
            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
            context.startActivity(intent);
        } else {
            result = do_print_current(context);
        }
        return result;
    }

    public static List<UsageStats> do_print_current(Context context) {
        List<UsageStats> apps = new ArrayList();
        try {
            UsageStatsManager usageStatsManager = (UsageStatsManager) context.getSystemService("usagestats");
            long duration = 24 * 60 * 60 * 1000;
            Date date = new Date();
            apps = usageStatsManager.queryUsageStats(
                       UsageStatsManager.INTERVAL_DAILY, 
                       date.getTime() - duration, date.getTime());
            Collections.sort(apps, new Comparator<UsageStats>() {
                @Override
                public int compare(UsageStats lhs, UsageStats rhs) {
                    return (int)(rhs.getLastTimeUsed() - lhs.getLastTimeUsed());
                }
            });

            for (UsageStats app: apps) {
                Date last_used = new Date(app.getLastTimeUsed());
                Log.i(TAG, "PackageName: " + last_used  + " " + app.getPackageName());
            }
            Log.i(TAG, "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return apps;
    }
}
