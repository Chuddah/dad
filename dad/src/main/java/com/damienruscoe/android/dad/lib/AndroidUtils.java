package com.damienruscoe.android.dad.lib;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

public class AndroidUtils {

    public static void show_message(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.show();
    }

    public static void start_termux(Context context) {
        start_activity(context, "com.termux");
    }

    public static void start_kodi(Context context) {
        start_activity(context, "org.xbmc.kodi");
    }

    public static void start_activity(Context context, String package_name) {
        PackageManager pm = context.getPackageManager();
        Intent i = pm.getLaunchIntentForPackage(package_name);
        context.startActivity(i);
    }

}

