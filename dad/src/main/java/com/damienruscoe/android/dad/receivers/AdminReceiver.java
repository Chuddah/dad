package com.damienruscoe.android.dad.recievers;

import android.content.Context;
import android.content.Intent;
import android.app.admin.DeviceAdminReceiver;
import android.util.Log;

import com.damienruscoe.android.dad.lib.AndroidUtils;
import com.damienruscoe.android.dad.lib.MqttUtils;

public class AdminReceiver extends DeviceAdminReceiver {

    static String Topic = "android/admin/receiver";

    @Override
    public void onEnabled(Context context, Intent intent) {
        MqttUtils.publish(context, Topic, "enabled");
    }

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        MqttUtils.publish(context, Topic, "disable_requested");
        return "disable_requested";
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        MqttUtils.publish(context, Topic, "disabled");
    }
}
